package com.yktkononov.testdatabase;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.yktkononov.testdatabase.fragments.EditFragment;
import com.yktkononov.testdatabase.fragments.MainFragment;
import com.yktkononov.testdatabase.fragments.ReviewFragment;
import com.yktkononov.testdatabase.instruments.MyDataBaseHelper;

public class MainActivity extends AppCompatActivity implements MainFragment.MainFragmentListener,
        EditFragment.EditFragmentListener, ReviewFragment.ReviewFragmentListener {

    private MyDataBaseHelper myDataBaseHelper;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        MainFragment fragment = new MainFragment();
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, fragment).commit();
    }

    @Override
    public void onClickItem(long vehicleId) {
        Fragment fragment = new ReviewFragment();
        Bundle args = new Bundle();
        args.putLong(MyDataBaseHelper.KEY_ID, vehicleId);
        fragment.setArguments(args);
        replaceFragment(fragment);
    }

    @Override
    public void onClickAdd() {
        Fragment fragment = new EditFragment();
        replaceFragment(fragment);
    }

    @Override
    public void onClickEdit(long vehicleId) {
        Fragment fragment = new EditFragment();
        Bundle args = new Bundle();
        args.putLong(MyDataBaseHelper.KEY_ID, vehicleId);
        fragment.setArguments(args);
        replaceFragment(fragment);
    }

    @Override
    public void onClickRemove(long vehicleId) {
        fragmentManager.popBackStack();
    }

    @Override
    public void onClickDone(){
        fragmentManager.popBackStack();
    }

    @Override
    public void onEditFragment() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public void onMainFragment() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    @Override
    public void onReviewFragment() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void replaceFragment(Fragment fragment){
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                .replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
