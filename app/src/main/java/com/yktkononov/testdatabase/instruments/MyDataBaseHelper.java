package com.yktkononov.testdatabase.instruments;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MyDataBaseHelper extends SQLiteOpenHelper {

    public static final int VERSION = 2;
    public static final String NAME = "myDataBase";
    public static final String TABLE_VEHICLES = "vehicles";

    public static final String KEY_ID = "_id";
    public static final String KEY_URL = "url";
    public static final String KEY_NAME = "name";

    private static MyDataBaseHelper instance = null;

//http://auto-offer.ru/u/dd/modelPhotos/10002/image.jpg

    private MyDataBaseHelper(Context context) {
        super(context, NAME, null, VERSION);
    }

    public static MyDataBaseHelper getInstance(Context context){
        if(instance == null){
            instance = new MyDataBaseHelper(context);
        }
        return instance;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + TABLE_VEHICLES + "(_id integer primary key, name text, url text)");

        for(int i = 0; i < 500; i++){
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_NAME, "vehicle " + (i + 1));
            contentValues.put(KEY_URL, "http://auto-offer.ru/u/dd/modelPhotos/" + (10000 + i + 1) + "/image.jpg");
            sqLiteDatabase.insert(TABLE_VEHICLES, null, contentValues);
        }
        Log.i("tag","Data Base created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_VEHICLES);
        onCreate(sqLiteDatabase);
    }

}
