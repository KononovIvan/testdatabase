package com.yktkononov.testdatabase.instruments;

import java.util.LinkedHashMap;
import java.util.Map;

public class FixedHashMap<K, V> extends LinkedHashMap<K, V> {

    private static final int MAX_SIZE = 100;

    @Override
    public V put(K key, V value) {
        return super.put(key, value);
    }

    @Override
    protected boolean removeEldestEntry(Entry<K, V> eldest) {
        return this.size() > MAX_SIZE;
    }
}
