package com.yktkononov.testdatabase;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yktkononov.testdatabase.instruments.FixedHashMap;

import java.util.List;
import java.util.Map;

public class DataAdapter extends BaseAdapter{

    private Context context;
    private LayoutInflater inflater;
    private List<Vehicle> vehicles;
    private static Map<String, Bitmap> imageMap = new FixedHashMap<>();

    public DataAdapter(Context context, List<Vehicle> items){
        this.context = context;
        this.vehicles = items;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addData(List<Vehicle> list){
        vehicles.addAll(list);
        this.notifyDataSetChanged();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MyViewHolder viewHolder;

        if(view == null){
            view = inflater.inflate(R.layout.list_item, viewGroup, false);
            viewHolder = new MyViewHolder();
            viewHolder.nameView = (TextView) view.findViewById(R.id.vehicle_name);
            viewHolder.imageView = (ImageView) view.findViewById(R.id.vehicle_image);
            view.setTag(viewHolder);
        }
        else{
            viewHolder = (MyViewHolder) view.getTag();
        }
        viewHolder.nameView.setText(getVehicle(i).getName());
        viewHolder.imageView.setImageResource(R.drawable.progress_bar);
        String url = getVehicle(i).getImageURL();
        if(imageMap.containsKey(url)) {
            viewHolder.stopDownloading();
            viewHolder.imageView.setImageBitmap(imageMap.get(url));
        }
        else {
            viewHolder.downloadImage(getVehicle(i).getImageURL());
        }

        return view;
    }


    @Override
    public int getCount() {
        return vehicles.size();
    }

    @Override
    public Object getItem(int i) {
        return vehicles.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public Vehicle getVehicle(int i){
        return (Vehicle) getItem(i);
    }

    class MyViewHolder implements DownloadImageTask.DownloadListener{
        TextView nameView;
        ImageView imageView;
        DownloadImageTask task = null;
        String url;

        void stopDownloading(){
            if(task != null){
                task.cancel(true);
                task = null;
            }
        }

        void downloadImage(String url){
            this.url = url;
            stopDownloading();
            task = new DownloadImageTask(this);
            task.execute(url);
        }

        @Override
        public void downloaded(Bitmap image) {
            imageView.setImageBitmap(image);
            imageMap.put(url, image);
        }
    }

}
