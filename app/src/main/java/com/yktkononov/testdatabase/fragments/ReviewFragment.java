package com.yktkononov.testdatabase.fragments;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yktkononov.testdatabase.DownloadImageTask;
import com.yktkononov.testdatabase.instruments.MyDataBaseHelper;
import com.yktkononov.testdatabase.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewFragment extends Fragment implements DownloadImageTask.DownloadListener {

    public interface ReviewFragmentListener{
        void onClickEdit(long vehicleId);
        void onClickRemove(long vehicleId);
        void onReviewFragment();
    }

    private DownloadImageTask downloadTask;
    private ReviewFragmentListener listener;
    private long vehicleId;
    private ImageView imageView;

    public ReviewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_review, container, false);

        FloatingActionButton fabEdit = (FloatingActionButton) view.findViewById(R.id.fabEdit);
        fabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null)
                    listener.onClickEdit(vehicleId);
            }
        });

        setHasOptionsMenu(true);

        //read data
        vehicleId = getArguments().getLong(MyDataBaseHelper.KEY_ID, 0);
        MyDataBaseHelper myDataBaseHelper = MyDataBaseHelper.getInstance(getContext());
        SQLiteDatabase db = myDataBaseHelper.getReadableDatabase();
        Cursor cursor = db.query(MyDataBaseHelper.TABLE_VEHICLES,
                null, MyDataBaseHelper.KEY_ID + "=" + vehicleId,
                null, null, null, null);
        int nameIndex = cursor.getColumnIndex(MyDataBaseHelper.KEY_NAME);
        int urlIndex = cursor.getColumnIndex(MyDataBaseHelper.KEY_URL);

        String name = "";
        String url = "";
        if(cursor.moveToFirst() && cursor.getCount() == 1){
            name = cursor.getString(nameIndex);
            url = cursor.getString(urlIndex);
        }
        else
            throw new RuntimeException("wrong element count " + cursor.getCount() + " with id = " + vehicleId);
        cursor.close();

        //init views
        imageView = (ImageView) view.findViewById(R.id.review_image_view);
        TextView textView = (TextView) view.findViewById(R.id.review_text_view);

        textView.setText(name);
        downloadTask = new DownloadImageTask(this);
        downloadTask.execute(url);

        if(listener != null)
            listener.onReviewFragment();

        return view;
    }

    @Override
    public void downloaded(Bitmap image) {
        imageView.setImageBitmap(image);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (ReviewFragmentListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onStop() {
        super.onStop();
        downloadTask.cancel(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_delete:
                MyDataBaseHelper myDataBaseHelper = MyDataBaseHelper.getInstance(getContext());
                SQLiteDatabase db = myDataBaseHelper.getWritableDatabase();
                db.delete(MyDataBaseHelper.TABLE_VEHICLES, MyDataBaseHelper.KEY_ID + "=" + vehicleId, null);
                Log.i("tag", "element with id=" + vehicleId + " deleted");
                if(listener != null)
                    listener.onClickRemove(vehicleId);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
