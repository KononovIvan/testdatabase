package com.yktkononov.testdatabase.fragments;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.yktkononov.testdatabase.instruments.MyDataBaseHelper;
import com.yktkononov.testdatabase.R;


public class EditFragment extends Fragment {

    public interface EditFragmentListener {
        void onClickDone();
        void onEditFragment();
    }

    private EditText editTextName;
    private EditText editTextURL;

    private long vehicleId;
    private String name = "name";
    private String url = "URL";

    private boolean isEditMode;

    private EditFragmentListener listener;

    public EditFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit, container, false);

        editTextName = (EditText) view.findViewById(R.id.editTextName);
        editTextURL = (EditText) view.findViewById(R.id.editTextURL);
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab3);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //do edit data base
                MyDataBaseHelper myDataBaseHelper = MyDataBaseHelper.getInstance(getContext());
                SQLiteDatabase db = myDataBaseHelper.getWritableDatabase();
                ContentValues contentValues = new ContentValues();
                contentValues.put(MyDataBaseHelper.KEY_NAME, editTextName.getText().toString());
                contentValues.put(MyDataBaseHelper.KEY_URL, editTextURL.getText().toString());
                if(isEditMode){
                    contentValues.put(MyDataBaseHelper.KEY_ID, vehicleId);
                    db.replace(MyDataBaseHelper.TABLE_VEHICLES, null, contentValues);
                    Log.i("tag", "element with id=" + vehicleId + " edited");
                }
                else{
                    db.insert(MyDataBaseHelper.TABLE_VEHICLES, null, contentValues);
                    Log.i("tag", "new element added");
                }
                if(listener != null)
                    listener.onClickDone();
            }
        });

        //read data
        if (getArguments() != null) {
            vehicleId = getArguments().getLong(MyDataBaseHelper.KEY_ID);//edit element with id
        }
        else
            vehicleId = -1;//add new data

        if(vehicleId != -1){//if edit mode
            isEditMode = true;
            MyDataBaseHelper myDataBaseHelper = MyDataBaseHelper.getInstance(getContext());
            SQLiteDatabase db = myDataBaseHelper.getReadableDatabase();
            Cursor cursor = db.query(MyDataBaseHelper.TABLE_VEHICLES,
                    null, MyDataBaseHelper.KEY_ID + "=" + vehicleId,
                    null, null, null, null);
            int nameIndex = cursor.getColumnIndex(MyDataBaseHelper.KEY_NAME);
            int urlIndex = cursor.getColumnIndex(MyDataBaseHelper.KEY_URL);

            if(cursor.moveToFirst() && cursor.getCount() == 1){
                name = cursor.getString(nameIndex);
                url = cursor.getString(urlIndex);
            }
            else
                throw new RuntimeException("wrong element count " + cursor.getCount() + " with id = " + vehicleId);
            cursor.close();
        }
        else
            isEditMode = false;


        editTextName.setText(name);
        editTextURL.setText(url);

        if(listener != null)
            listener.onEditFragment();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EditFragmentListener) {
            listener = (EditFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

}
