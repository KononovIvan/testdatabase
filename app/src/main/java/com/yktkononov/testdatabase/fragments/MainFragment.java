package com.yktkononov.testdatabase.fragments;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.yktkononov.testdatabase.DataAdapter;
import com.yktkononov.testdatabase.instruments.MyDataBaseHelper;
import com.yktkononov.testdatabase.R;
import com.yktkononov.testdatabase.Vehicle;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    public interface MainFragmentListener{
        void onClickItem(long vehicleId);
        void onClickAdd();
        void onMainFragment();
    }

    private static final int FILLING = 0;
    private static final int FILLED = 1;
    private static final int PAGE_SIZE = 10;

    private View footerView;
    private ListView listView;
    private DataAdapter adapter;
    private boolean isLoading = false;
    private MyHandler handler;
    private int currentPage = 0;
    private MyDataBaseHelper myDataBaseHelper;
    private SQLiteDatabase db;
    private boolean isListEnd;
    private MainFragmentListener listener;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        currentPage = 0;
        isListEnd = false;
        myDataBaseHelper = MyDataBaseHelper.getInstance(getContext());
        db = myDataBaseHelper.getReadableDatabase();
        listView = (ListView) view.findViewById(R.id.list_view);
        footerView = inflater.inflate(R.layout.footer_view, container, false);
        adapter = new DataAdapter(getContext(), getPageDataList(currentPage++));
        handler = new MyHandler();
        listView.setAdapter(adapter);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                if(absListView.getLastVisiblePosition() == i2 - 1 &&
                        listView.getCount() >= PAGE_SIZE && !isLoading && !isListEnd){
                    isLoading = true;
                    Thread thread = new GetNextDataThread();
                    thread.start();
                }
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(listener != null)
                    listener.onClickItem(adapter.getVehicle(i).getId());
            }
        });

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab1);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null)
                    listener.onClickAdd();
            }
        });

        if(listener != null)
            listener.onMainFragment();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof MainFragmentListener)
            listener = (MainFragmentListener) context;
        else
            throw new RuntimeException(context.getClass().getSimpleName() + " must be MainFragmentListener");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private List<Vehicle> getPageDataList(int page){
        List<Vehicle> items = new ArrayList<>();
        Cursor cursor = db.query(MyDataBaseHelper.TABLE_VEHICLES, null, null,
                null, null, null, null);
        if(cursor.moveToPosition(page * PAGE_SIZE)){
            int idIndex = cursor.getColumnIndex(MyDataBaseHelper.KEY_ID);
            int nameIndex = cursor.getColumnIndex(MyDataBaseHelper.KEY_NAME);
            int urlIndex = cursor.getColumnIndex(MyDataBaseHelper.KEY_URL);
            int i = 0;
            do{
                Vehicle vehicle = new Vehicle(cursor.getLong(idIndex), cursor.getString(nameIndex), cursor.getString(urlIndex));
                items.add(vehicle);
                i++;
            }while(cursor.moveToNext() && i < 10);
            Log.i("tag","Data Base read complete");
        }
        else{
            isListEnd = true;
            Log.i("tag","no more Data");
        }
        cursor.close();
        return items;
    }

    class MyHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case FILLING://filling new page
                    listView.addFooterView(footerView);
                    break;
                case FILLED://filled
                    listView.removeFooterView(footerView);
                    adapter.addData((List<Vehicle>)msg.obj);
                    isLoading = false;
                    break;
            }
        }
    }

    class GetNextDataThread extends Thread{
        @Override
        public void run() {
            handler.sendEmptyMessage(FILLING);

            List<Vehicle> list = getPageDataList(currentPage++);

            /*try {
                Thread.sleep(1000); //imitating heavy load
            }catch (Exception e){
                e.printStackTrace();
            }*/
            Message msg = handler.obtainMessage(FILLED, list);
            handler.sendMessage(msg);
        }
    }
}
