package com.yktkononov.testdatabase;

public class Vehicle {

    private long id;
    private String name;
    private String imageURL;

    public Vehicle(long id, String name, String imageURL) {
        this.id = id;
        this.name = name;
        this.imageURL = imageURL;
    }

    public long getId(){
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImageURL() {
        return imageURL;
    }
}
